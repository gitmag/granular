package granular.site.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for the greeting page!
 */
@Controller
public class GreetingController {

    // This returns the greeting html page and substitutes the specified mappings into the html template at th:x...
    @GetMapping("/greeting")
    public String greeting(@RequestParam (value="name", required=false, defaultValue = "world") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }
}
