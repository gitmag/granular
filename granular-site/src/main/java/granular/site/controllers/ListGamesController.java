package granular.site.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mag on 2/26/17.
 */
@Controller
public class ListGamesController {

    @GetMapping("/game/list_games")
    public String listGames() {
        return "list_games";
    }
}
