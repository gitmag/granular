package granular.site.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by mag on 2/26/17.
 */
@Controller
@RequestMapping("game/add_review")
public class AddReviewController {
    @GetMapping
    public String addReview() {
        return "add_review";
    }

    @PostMapping
    public String reviewSubmitted() {
        return "index";
    }
}
