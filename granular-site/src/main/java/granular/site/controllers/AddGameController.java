package granular.site.controllers;

import granular.interactor.SaveGameInteractor;
import granular.site.models.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for adding a game
 */
@Controller
@RequestMapping(value="/game/add_game")
public class AddGameController implements SaveGameInteractor.Callback {
    private SaveGameInteractor saveGameInteractor;
    private SaveGameInteractor.Callback callback;

    @Autowired
    public AddGameController(SaveGameInteractor saveGameInteractor) {
        this.saveGameInteractor = saveGameInteractor;
    }

    @GetMapping
    public String addGame(Model model) {
        model.addAttribute("game", new Game());
        return "add_game";
    }

    @PostMapping
    public String addGamePost(Game game) {
        // Convert the model here to a domain model for Game
        saveGameInteractor.setCallback(this);

        try {
            saveGameInteractor.saveGame(null);
        } catch (RuntimeException e) {
            // TODO Loggin!
        }
        // Redirect to game list or maybe game detail of added game
        return "redirect:/game/list_games";
    }

    @Override
    public void onGameSaved(String message) {
        // TODO redirect here?
    }
}
