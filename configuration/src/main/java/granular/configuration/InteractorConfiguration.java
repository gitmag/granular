package granular.configuration;

import granular.domain.repository.GameRepository;
import granular.interactor.SaveGameInteractor;
import granular.interactor.impl.SaveGameInteractorImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by mag on 2/24/17.
 */
@Configuration
public class InteractorConfiguration {
    // TODO REDo interface callbacks
    @Bean
    SaveGameInteractor saveGameInteractor(GameRepository gameRepository) {
        return new SaveGameInteractorImpl(gameRepository);
    }



}
