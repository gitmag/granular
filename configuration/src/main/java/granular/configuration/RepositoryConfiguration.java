package granular.configuration;

import granular.domain.model.Game;
import granular.domain.model.Review;
import granular.domain.model.User;
import granular.domain.repository.GameRepository;
import granular.domain.repository.ReviewRepository;
import granular.domain.repository.UserRepository;
import granular.storage.redis.GameRepositoryImpl;
import granular.storage.redis.ReviewRepositoryImpl;
import granular.storage.redis.UserRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by mag on 2/25/17.
 */

public class RepositoryConfiguration {
    @Bean
    public GameRepository gameRepository(RedisTemplate<String, Game> redisTemplate) {
        return new GameRepositoryImpl(redisTemplate);
    }

    @Bean
    public UserRepository userRepository(RedisTemplate<String, User> redisTemplate) {
        return new UserRepositoryImpl(redisTemplate);
    }

    @Bean
    public ReviewRepository reviewRepository(RedisTemplate<String, Review> redisTemplate) {
        return new ReviewRepositoryImpl(redisTemplate);
    }

}
