package granular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by mag on 2/4/17.
 */

@SpringBootApplication(scanBasePackages={"granular.configuration", "granular.site", "granular.storage"})
public class GranularApplication {

    // Main method picked up automatically with SpringBootApplication annotation.
    public static void main(String... args) {
        SpringApplication.run(GranularApplication.class, args);
    }
}
