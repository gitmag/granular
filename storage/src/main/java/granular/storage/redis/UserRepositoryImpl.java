package granular.storage.redis;

import granular.domain.model.User;
import granular.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by mag on 2/8/17.
 */
@Repository
public class UserRepositoryImpl implements UserRepository {
    private static final String KEY = "user";
    private RedisTemplate<String, User> redisTemplate;

    @Autowired
    public UserRepositoryImpl(RedisTemplate<String, User> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // Used for redis hash operations
    @Resource(name="redisTemplate")
    private HashOperations<String, String, User> hashOps;


    @Override
    public void addUser(User user) {
        hashOps.put(KEY, user.getId(), user);
    }

    @Override
    public void updateUser(User user) {

    }

    @Override
    public void removeUser(User user) {

    }

    @Override
    public List<User> query() {
        return null;
    }
}
