package granular.storage.redis;

import granular.domain.model.Game;
import granular.domain.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


/**
 * Repository for using redis to store game objects
 */
@Repository
public class GameRepositoryImpl implements GameRepository {
    // The beginning of the key for redis
    private static final String KEY= "game";
    private RedisTemplate<String, Game> redisTemplate;


    @Autowired
    public GameRepositoryImpl( RedisTemplate<String, Game> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // Used for redis hash operations
    @Resource(name="redisTemplate")
    private HashOperations<String, String, Game> hashOps;

    @Override
    public void addGame(Game game) {
        hashOps.put(KEY, game.getId(), game);
    }

    @Override
    public void updatGame(Game game) {
        hashOps.put(KEY, game.getId(), game);
    }

    @Override
    public void removeGame(Game game) {
        hashOps.delete(KEY, game.getId());
    }

    @Override
    public List<Game> query() {
        return null;
    }
}
