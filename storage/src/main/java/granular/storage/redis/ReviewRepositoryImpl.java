package granular.storage.redis;

import granular.domain.model.Review;
import granular.domain.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ReviewRepositoryImpl implements ReviewRepository {
    private static final String KEY = "review";

    private RedisTemplate<String, Review> redisTemplate;

    @Autowired
    public ReviewRepositoryImpl(RedisTemplate<String, Review> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // Used for redis hash operations
    @Resource(name="redisTemplate")
    private HashOperations<String, String, Review> hashOps;


    @Override
    public void addReview(Review review) {
        hashOps.put(KEY, review.getId(), review);
    }

    @Override
    public void updateReview(Review review) {
        hashOps.put(KEY, review.getId(), review);
    }

    @Override
    public void removeReview(Review review) {
        hashOps.delete(KEY, review.getId());
    }

    @Override
    public List<Review> query() {
        return null;
    }
}
