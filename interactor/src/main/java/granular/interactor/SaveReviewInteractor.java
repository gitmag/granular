package granular.interactor;

import granular.domain.model.Review;

public interface SaveReviewInteractor {

    void save(Review review);

    interface Callback {
        void onReviewSaved();
    }
}