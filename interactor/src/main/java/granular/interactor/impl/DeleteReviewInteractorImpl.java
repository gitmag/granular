package granular.interactor.impl;

import granular.domain.model.Review;
import granular.domain.repository.ReviewRepository;
import granular.interactor.DeleteReviewInteractor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by mag on 2/23/17.
 */
public class DeleteReviewInteractorImpl implements DeleteReviewInteractor {
    private DeleteReviewInteractor.Callback callback;
    private ReviewRepository reviewRepository;

    @Autowired
    public DeleteReviewInteractorImpl(ReviewRepository reviewRepository, Callback callback) {
        this.reviewRepository = reviewRepository;
        this.callback = callback;
    }

    @Override
    public void delete(Review review) {
        reviewRepository.removeReview(review);
        callback.onDelete();
    }
}
