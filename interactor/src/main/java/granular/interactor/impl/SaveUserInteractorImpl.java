package granular.interactor.impl;

import granular.domain.model.User;
import granular.domain.repository.UserRepository;
import granular.interactor.SaveUserInteractor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by mag on 2/23/17.
 */
public class SaveUserInteractorImpl implements SaveUserInteractor {
    private SaveUserInteractor.Callback callback;
    private UserRepository userRepository;

    @Autowired
    public SaveUserInteractorImpl(UserRepository userRepository, Callback callback) {
        this.userRepository = userRepository;
        this.callback = callback;
    }

    @Override
    public void save(User user) {
        userRepository.updateUser(user);
        callback.onUserSaved();
    }
}
