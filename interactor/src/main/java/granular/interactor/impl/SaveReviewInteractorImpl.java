package granular.interactor.impl;

import granular.domain.model.Review;
import granular.domain.repository.ReviewRepository;
import granular.interactor.SaveReviewInteractor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by mag on 2/18/17.
 */
public class SaveReviewInteractorImpl implements SaveReviewInteractor {
    private SaveReviewInteractor.Callback callback;
    private ReviewRepository reviewRepository;

    @Autowired
    public SaveReviewInteractorImpl(ReviewRepository reviewRepository, Callback callback) {
        this.reviewRepository = reviewRepository;
        this.callback = callback;
    }

    @Override
    public void save(Review review) {
        // Insert/Update the review in the repository
        reviewRepository.updateReview(review);
        // Use the callback so the requesting object knows it happened
        callback.onReviewSaved();
    }

}
