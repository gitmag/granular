package granular.interactor.impl;

import granular.domain.model.Game;
import granular.domain.repository.GameRepository;
import granular.interactor.SaveGameInteractor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by mag on 2/23/17.
 */
public class SaveGameInteractorImpl implements SaveGameInteractor {
    private SaveGameInteractor.Callback callback;
    private GameRepository gameRepository;

    @Autowired
    public SaveGameInteractorImpl(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void setCallback(SaveGameInteractor.Callback callback) {
        this.callback = callback;
    }

    @Override
    public void saveGame(Game game) {
        gameRepository.updatGame(game);
        callback.onGameSaved("game saved successfully!");
    }
}
