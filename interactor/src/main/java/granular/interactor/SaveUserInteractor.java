package granular.interactor;

import granular.domain.model.User;

/**
 * Created by mag on 2/20/17.
 */
public interface SaveUserInteractor {

    void save(User user);

    interface Callback {
        void onUserSaved();
    }
}
