package granular.interactor;

import granular.domain.model.Game;

/**
 * Interactor = Story = Runner = Do things related to the domain
 * This is an interactor for saving a game
 */
public interface SaveGameInteractor {

    // TODO Possibly these should take the necessary items, as opposed to the Game object itself
    // TODO this probalby needs to do some conversion from a model in the MVC realm to a domain model
    /**
     * Save a game
     * @param game
     */
    void saveGame(Game game);

    /**
     * Callback for when a game is saved/attempted to save
     */
    interface Callback {
        void onGameSaved(String message);
    }

    void setCallback(Callback callback);
}
