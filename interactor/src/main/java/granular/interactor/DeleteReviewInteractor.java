package granular.interactor;

import granular.domain.model.Review;

/**
 * Created by mag on 2/20/17.
 */
public interface DeleteReviewInteractor {

    void delete(Review review);

    interface Callback {
        void onDelete();
    }
}
