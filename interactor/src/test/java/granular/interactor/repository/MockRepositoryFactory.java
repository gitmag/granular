package granular.interactor.repository;

import granular.domain.model.Game;
import granular.domain.model.Review;
import granular.domain.model.User;
import granular.domain.repository.GameRepository;
import granular.domain.repository.ReviewRepository;
import granular.domain.repository.UserRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implement repository interfaces using maps for quick testing
 */
public class MockRepositoryFactory {

    public static GameRepository createMockGameRepository(Map<String, Game> fakeRepository) {
        return new GameRepository() {
            @Override
            public void addGame(Game game) {
                fakeRepository.put(game.getId(), game);
            }

            @Override
            public void updatGame(Game game) {
                fakeRepository.put(game.getId(), game);
            }

            @Override
            public void removeGame(Game game) {
                fakeRepository.remove(game.getId());
            }

            @Override
            public List<Game> query() {
                return new ArrayList<Game>(fakeRepository.values());
            }
        };
    }

    public static UserRepository createMockUserRepository(Map<String, User> fakeRepository) {
        return new UserRepository() {
            @Override
            public void addUser(User user) {
                fakeRepository.put(user.getId(), user);
            }

            @Override
            public void updateUser(User user) {
                fakeRepository.put(user.getId(), user);
            }

            @Override
            public void removeUser(User user) {
                fakeRepository.remove(user.getId());
            }

            @Override
            public List<User> query() {
                return new ArrayList<User>(fakeRepository.values());
            }
        };
    }

    public static ReviewRepository createMockReviewRepository(Map<String, Review> fakeRepository) {
        return new ReviewRepository() {
            @Override
            public void addReview(Review review) {
                fakeRepository.put(review.getId(), review);
            }

            @Override
            public void updateReview(Review review) {
                fakeRepository.put(review.getId(), review);
            }

            @Override
            public void removeReview(Review review) {
                fakeRepository.remove(review.getId());
            }

            @Override
            public List<Review> query() {
                return new ArrayList<Review>(fakeRepository.values());
            }
        };
    }
}
