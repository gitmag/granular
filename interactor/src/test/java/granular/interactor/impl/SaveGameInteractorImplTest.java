package granular.interactor.impl;

import granular.domain.model.Game;
import granular.interactor.SaveGameInteractor;
import granular.interactor.repository.MockRepositoryFactory;
import junit.framework.TestCase;

import java.util.*;

/**
 * Created by mag on 2/24/17.
 */
public class SaveGameInteractorImplTest extends TestCase {
    private Map<String, Game> fakeRepository;
    private SaveGameInteractor saveGameInteractor;
    private SaveGameInteractor.Callback callback;
    private String message;


    @org.junit.Before
    public void setUp() throws Exception {
        fakeRepository = new HashMap<>();
        callback = (String s) -> {message = s;};
        saveGameInteractor = new SaveGameInteractorImpl(MockRepositoryFactory.createMockGameRepository(fakeRepository));
        saveGameInteractor.setCallback(callback);
    }

    @org.junit.After
    public void tearDown() throws Exception {
        fakeRepository = null;
        saveGameInteractor = null;
        callback = null;
    }

    public void testSaveGame() throws Exception {
        Game game = new Game("ferjgzloidf","Sonic", "", new Date(System.currentTimeMillis()), "");
        saveGameInteractor.saveGame(game);
        assertTrue(fakeRepository.get(game.getId())!= null);
        assertEquals(message, "game saved successfully!");
    }

}