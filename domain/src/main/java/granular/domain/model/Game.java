package granular.domain.model;

import granular.domain.AssertionConcern;

import java.util.Date;
import java.util.UUID;

public class Game extends AssertionConcern {
    private String id;
    private String title;
    private String publisher;
    private Date publicationDate;
    private String description;

    // This is for creating a new game object from an existing game
    public Game(String id, String title, String publisher, Date publicationDate, String description) {
        this.id = id;
        this.title = title;
        this.publisher = publisher;
        this.publicationDate = publicationDate;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.assertArgumentNotEmpty(id, "Game id must not be empty");
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.assertArgumentNotEmpty(title, "Game title must not be empty");
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.assertArgumentNotEmpty(description, "Game description must not be empty");
        this.description = description;
    }

}
