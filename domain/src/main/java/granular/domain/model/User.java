package granular.domain.model;

import granular.domain.AssertionConcern;

import java.util.UUID;

public class User extends AssertionConcern {
    private String id;
    private String username;
    private String password;
    private String catchPhrase;

    // Used for creation of this object from an existing user
    public User(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.assertArgumentNotEmpty(id, "User id must not be empty");
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }
    
}
