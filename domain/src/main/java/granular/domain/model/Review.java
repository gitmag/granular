package granular.domain.model;

import granular.domain.AssertionConcern;

import java.util.UUID;

public class Review extends AssertionConcern {
    private String id;
    private String idGame;
    private String idUser;
    private String title;
    private String review;

    // This is for creating an instance of an already existing review
    public Review(String id, String idGame, String idUser, String title, String review) {
        this.id = id;
        this.idGame = idGame;
        this.idUser = idUser;
        this.title = title;
        this.review = review;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.assertArgumentNotEmpty(id, "Review id must not be empty");
        this.id = id;
    }

    public String getIdGame() {
        return idGame;
    }

    public void setIdGame(String idGame) {
        this.assertArgumentNotEmpty(idGame, "Game id must not be empty");
        this.idGame = idGame;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.assertArgumentNotEmpty(idUser, "User id must not be empty");
        this.idUser = idUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }


}
