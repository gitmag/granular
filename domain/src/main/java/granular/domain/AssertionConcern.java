package granular.domain;

/**
 * Used for validation/assertion of model things
 */
public class AssertionConcern {

    protected AssertionConcern() {
        super();
    }

    protected void assertArgumentLength(String string, int max, String message) {
        int length = string.trim().length();
        if (length > max) {
            throw new IllegalArgumentException(message);
        }
    }

    protected void assertArgumentNotEmpty(String string, String message) {
        if (string == null || string.trim().isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

}
