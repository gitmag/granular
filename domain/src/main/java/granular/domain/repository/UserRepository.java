package granular.domain.repository;

import granular.domain.model.User;

import java.util.List;

/**
 * Created by mag on 2/8/17.
 */
public interface UserRepository {
    void addUser(User user);

    void updateUser(User user);

    void removeUser(User user);

    List<User> query();

}
