package granular.domain.repository;

import granular.domain.model.Game;

import java.util.List;

/**
 * Created by mag on 2/8/17.
 */
public interface GameRepository {
    void addGame(Game game);

    void updatGame(Game game);

    void removeGame(Game game);

    List<Game> query();
}
