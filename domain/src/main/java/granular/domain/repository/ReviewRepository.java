package granular.domain.repository;

import granular.domain.model.Review;

import java.util.List;

/**
 * Created by mag on 2/8/17.
 */
public interface ReviewRepository {
    void addReview(Review review);

    void updateReview(Review review);

    void removeReview(Review review);

    List<Review> query();
}
